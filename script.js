"use strict"
$(document).ready(function(){
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 14,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        }
    ]
}
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    isPopular();
    isTrending();
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function isPopular(){
    "use strict"
    //Bước 1: Tạo object chứa dữ liệu :
    var vPopularCoursesArray = [];
    //Bước 2: Thu thập dữ liệu :
    getPopularCourses(vPopularCoursesArray);
    console.log("Mảng các khoá học trong mục popular là :");
    console.log(vPopularCoursesArray);
    //Bước 3: Validate (kiểm tra) dữ liệu : (không có)
    //Bước 4: Hiển thị dữ liệu :
    showPopularCourses(vPopularCoursesArray);
}
function isTrending(){
    "use strict"
    //Bước 1: Tạo object chứa dữ liệu :
    var vTrendingCoursesArray = [];
    //Bước 2: Thu thập dữ liệu :
    getTrendingCourses(vTrendingCoursesArray);
    console.log("Mảng các khoá học trong mục trending là :");
    console.log(vTrendingCoursesArray);
    //Bước 3: Validate (kiểm tra) dữ liệu : (không có)
    //Bước 4: Hiển thị dữ liệu :
    showTrendingCourses(vTrendingCoursesArray);
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Hàm lấy danh sách các khoá học ở mục popular :
function getPopularCourses(paramPopularCoursesArray){
    "use strict"
    for(var vIndex = 0 ; vIndex < gCoursesDB.courses.length ; vIndex++){
        if(gCoursesDB.courses[vIndex].isPopular === true){
            paramPopularCoursesArray.push(gCoursesDB.courses[vIndex]);
        }
    }
}
//Hàm hiển thị các khoá học ra mục popular :
function showPopularCourses(paramPopularCoursesArray){
    "use strict"
    var vDivPopular = $(".popular");
    for(var vIndex = 0 ; vIndex < paramPopularCoursesArray.length ; vIndex ++){
        var vStr = `
              <div class="col-sm-3">
                <div class="card mb-4 card-hover">
                  <a href="" class="card-img-top">
                    <img src="`+ paramPopularCoursesArray[vIndex].coverImage +`" alt="" class="rounded-top card-img-top">
                  </a>
                  <div class="card-body">
                    <h6 class="mb-2">
                      <a href="">`+ paramPopularCoursesArray[vIndex].courseName +`</a>
                    </h6>
                    <ul class="mb-3 list-inline">
                      <li class="list-inline-item">
                        <i class="fa fa-clock mr-2"></i>
                        "`+ paramPopularCoursesArray[vIndex].duration +`"
                      </li>
                      <li class="list-inline-item">`+ paramPopularCoursesArray[vIndex].level +`</li>
                    </ul>
                    <div class="mt-3">
                      <span class="text-dark font-weight-bold">$`+ paramPopularCoursesArray[vIndex].discountPrice +`</span>
                      <del class="text-muted">$`+ paramPopularCoursesArray[vIndex].price +`</del>
                    </div>
                  </div>
                  <div class="card-footer">
                    <div class="float-left mr-3">
                      <img src="`+ paramPopularCoursesArray[vIndex].teacherPhoto +`" alt="" class="rounded-circle img-teacher-avatar">
                    </div>
                    <div class="float-left">
                      <small>`+ paramPopularCoursesArray[vIndex].teacherName +`</small>
                    </div>
                    <div class="float-right">
                      <i class="fa fa-bookmark text-secondary"></i>
                    </div>
                  </div>
                </div>
       `
       vDivPopular.append(vStr);
    }
}
//Hàm lấy danh sách các khoá học ở mục trending :
function getTrendingCourses(paramTrendingCoursesArray){
    "use strict"
    for(var vIndex = 0 ; vIndex < gCoursesDB.courses.length ; vIndex++){
        if(gCoursesDB.courses[vIndex].isTrending === true){
            paramTrendingCoursesArray.push(gCoursesDB.courses[vIndex]);
        }
    }
}
//Hàm hiển thị các khoá học ở mục trending:
function showTrendingCourses(paramTrendingCoursesArray){
    "use strict"
    var vDivTrending = $(".trending");
    for(var vIndex = 0 ; vIndex < paramTrendingCoursesArray.length ; vIndex ++){
        var vStr = `
              <div class="col-sm-3">
                <div class="card mb-4 card-hover">
                  <a href="" class="card-img-top">
                    <img src="`+ paramTrendingCoursesArray[vIndex].coverImage +`" alt="" class="rounded-top card-img-top">
                  </a>
                  <div class="card-body">
                    <h6 class="mb-2">
                      <a href="">`+ paramTrendingCoursesArray[vIndex].courseName +`</a>
                    </h6>
                    <ul class="mb-3 list-inline">
                      <li class="list-inline-item">
                        <i class="fa fa-clock mr-2"></i>
                        "`+ paramTrendingCoursesArray[vIndex].duration +`"
                      </li>
                      <li class="list-inline-item">`+ paramTrendingCoursesArray[vIndex].level +`</li>
                    </ul>
                    <div class="mt-3">
                      <span class="text-dark font-weight-bold">$`+ paramTrendingCoursesArray[vIndex].discountPrice +`</span>
                      <del class="text-muted">$`+ paramTrendingCoursesArray[vIndex].price +`</del>
                    </div>
                  </div>
                  <div class="card-footer">
                    <div class="float-left mr-3">
                      <img src="`+ paramTrendingCoursesArray[vIndex].teacherPhoto +`" alt="" class="rounded-circle img-teacher-avatar">
                    </div>
                    <div class="float-left">
                      <small>`+ paramTrendingCoursesArray[vIndex].teacherName +`</small>
                    </div>
                    <div class="float-right">
                      <i class="fa fa-bookmark text-secondary"></i>
                    </div>
                  </div>
                </div>
       `
       vDivTrending.append(vStr);
    }
}
});

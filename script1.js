"use strict"
$(document).ready(function(){
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    //Biến chứa thông tin các khoá học :
    var gCoursesDB = {
        description: "This DB includes all courses in system",
        courses: [
            {
                id: 1,
                courseCode: "FE_WEB_ANGULAR_101",
                courseName: "How to easily create a website with Angular",
                price: 750,
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-angular.jpg",
                teacherName: "Morris Mccoy",
                teacherPhoto: "images/teacher/morris_mccoy.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 2,
                courseCode: "BE_WEB_PYTHON_301",
                courseName: "The Python Course: build web application",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-python.jpg",
                teacherName: "Claire Robertson",
                teacherPhoto: "images/teacher/claire_robertson.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 5,
                courseCode: "FE_WEB_GRAPHQL_104",
                courseName: "GraphQL: introduction to graphQL for beginners",
                price: 850,
                discountPrice: 650,
                duration: "2h 15m",
                level: "Intermediate",
                coverImage: "images/courses/course-graphql.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: false
            },
            {
                id: 6,
                courseCode: "FE_WEB_JS_210",
                courseName: "Getting Started with JavaScript",
                price: 550,
                discountPrice: 300,
                duration: "3h 34m",
                level: "Beginner",
                coverImage: "images/courses/course-javascript.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 8,
                courseCode: "FE_WEB_CSS_111",
                price: 750,
                    courseName: "CSS: ultimate CSS course from beginner to advanced",
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-css.jpg",
                teacherName: "Juanita Bell",
                teacherPhoto: "images/teacher/juanita_bell.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 14,
                courseCode: "FE_WEB_WORDPRESS_111",
                courseName: "Complete Wordpress themes & plugins",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-wordpress.jpg",
                teacherName: "Clevaio Simon",
                teacherPhoto: "images/teacher/clevaio_simon.jpg",
                isPopular: true,
                isTrending: false
            }
        ]
    }
    //
    const gID_COL = 0;
    const gCOURSE_CODE_COL = 1;
    const gCOURSE_NAME_COL = 2;
    const gPRICE_COL = 3;
    const gDISCOUNT_PRICE_COL = 4;
    const gDURATION_COL = 5;
    const gLEVEL_COL = 6;
    const gCOVER_IMAGE_COL = 7;
    const gTEACHER_NAME_COL = 8;
    const gTEACHER_PHOTOP_COL = 9;
    const gIS_POPULAR_COL = 10;
    const gIS_TRENDING_COL = 11;
    const gACTION_COL = 12;
    //Biến mảng hằng số chứa danh sách tên các thuộc tính
    const gNameCol = ["id","courseCode","courseName","price","discountPrice","duration","level","coverImage","teacherName","teacherPhoto","isPopular","isTrending","action"];
    //Khai báo Data Table và mapping columns 
    var vDataTable = $("#table").DataTable({
        columns: [
            {data: gNameCol[gID_COL]},
            {data: gNameCol[gCOURSE_CODE_COL]},
            {data: gNameCol[gCOURSE_NAME_COL]},
            {data: gNameCol[gPRICE_COL]},
            {data: gNameCol[gDISCOUNT_PRICE_COL]},
            {data: gNameCol[gDURATION_COL]},
            {data: gNameCol[gLEVEL_COL]},
            {data: gNameCol[gCOVER_IMAGE_COL]},
            {data: gNameCol[gTEACHER_NAME_COL]},
            {data: gNameCol[gTEACHER_PHOTOP_COL]},
            {data: gNameCol[gIS_POPULAR_COL]},
            {data: gNameCol[gIS_TRENDING_COL]},
            {data: gNameCol[gACTION_COL]},
        ],
        columnDefs:[
            {
                targets: gACTION_COL,
                defaultContent:"<button class='btn btn-success btn-edit'>Sửa</button><button class='btn btn-danger btn-delete'>Xoá</button>"
            },
            {
                targets: gCOVER_IMAGE_COL,
                render: function(data,type,row){
                    return '<img src="'+ data+'",width=60px, height=60px />'
                },
            },
            {
                targets: gTEACHER_PHOTOP_COL,
                render: function(data,type,row){
                    return '<img src="'+ data+'",width=60px, height=60px />'
                },
            }
        ]
    });
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    $(document).on("click",".btn-edit",function(){
        onBtnEditClick(this);
    });
    //Hàm xử lý sự kiện khi bấm nút đồng ý trong modal edit:
    $("#btn-accept-edit").on("click",function(){
        onBtnAccecptEditClick()
    });
    //Hàm xử lý sự kiện khi bấm nút huỷ trong modal delete:
    $("#btn-cancel-edit").on("click",function(){
        $("#edit-modal").modal("hide");
    });
    //Hàm xử lý sự kiện khi bấm nút Xoá 
    $(document).on("click",".btn-delete",function(){
        onBtnDeleteClick(this);
    });
    //Hàm xử lý sự kiện khi bấm nút đồng ý trong modal delete :
    $("#btn-accept-delete").on("click",function(){
        onBtnAccecptDeleteClick();
    });
    //Hàm xử lý sự kiện khi bấm nút huỷ trong modal delete :
    $("#btn-cancel-delete").on("click",function(){
        $("#delete-modal").modal("hide");
    })
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    //hàm tải trang:
    function onPageLoading(){
        "use strict"
        loadDataCourseToTable();
    }
    //Hàm xử lý sự kiện khi bấm bút Edit :
    function onBtnEditClick(paramButton){
        "use strict"
        $("#edit-modal").modal("show");
        var vRowClick = $(paramButton).closest("tr");
        var vTable = $("#table").DataTable();
        var vDataRow = vTable.row(vRowClick).data();
        console.log(vDataRow);
        //Load thông tin khoá học lên form :
        loadDataCourseToFormModal(vDataRow);
    }
    //Hàm xử lý sự kiện khi bấm nút đồng ý trong modal edit ( sửa thông tin khoá học )
    function onBtnAccecptEditClick(){
        "use strict"
        //Bước 1 : Tạo object chứa thông tin khoá học :
        var vDataCourseObject = {
            id:"",
            courseCode:"",
            courseName:"",
            coverImage:"",
            discountPrice:"",
            duration:"",
            isPopular:"",
            isTrending:"",
            level:"",
            price:"",
            teacherName:"",
            teacherPhoto:"",
        }
        //Bước 2 : Thu thập thông tin :
        getDataCourse(vDataCourseObject);
        console.log(vDataCourseObject);
        //Bước 3 : Kiểm tra (validate) dữ liệu :
        if(validateDataCourse(vDataCourseObject)){
            //Bước 4: Sửa giá trị :
            getDataEditToCourseObject(vDataCourseObject);
        }
    }
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm load danh sách khoá học lên table :
    function loadDataCourseToTable(){
        "use strict"
        vDataTable.clear();
        vDataTable.rows.add(gCoursesDB.courses);
        vDataTable.draw();
    }
    //Hàm tải thông tin khoá học lên form modal edit :
    function loadDataCourseToFormModal(paramDataRow){
        "use strict"
        $("#inp-id").val(paramDataRow.id);
        $("#inp-courseCode").val(paramDataRow.courseCode);
        $("#inp-courseName").val(paramDataRow.courseName);
        $("#inp-price").val(paramDataRow.price);
        $("#inp-discountPrice").val(paramDataRow.discountPrice);
        $("#inp-duration").val(paramDataRow.duration);
        $("#inp-level").val(paramDataRow.level);
        $("#inp-coverImage").val(paramDataRow.coverImage);
        $("#inp-teacherName").val(paramDataRow.teacherName);
        $("#inp-teacherPhoto").val(paramDataRow.teacherPhoto);
        $("#inp-isPopular").val(paramDataRow.isPopular);
        $("#inp-isTrending").val(paramDataRow.isTrending);
    }
    //Hàm lấy thông tin khoá học từ form modal vào object :
    function getDataCourse(paramDataCourseObject){
        "use strict"
        paramDataCourseObject.id = parseInt($("#inp-id").val().trim());
        paramDataCourseObject.courseCode = $("#inp-courseCode").val().trim();
        paramDataCourseObject.courseName = $("#inp-courseName").val().trim();
        paramDataCourseObject.coverImage = $("#inp-coverImage").val().trim();
        paramDataCourseObject.discountPrice = parseInt($("#inp-discountPrice").val().trim());
        paramDataCourseObject.duration = $("#inp-duration").val().trim();
        paramDataCourseObject.isPopular =  Boolean($("#inp-isPopular").val());
        paramDataCourseObject.isTrending = Boolean($("#inp-isTrending").val());
        paramDataCourseObject.level = $("#inp-level").val().trim();
        paramDataCourseObject.price = parseInt($("#inp-price").val().trim());
        paramDataCourseObject.teacherName = $("#inp-teacherName").val().trim();
        paramDataCourseObject.teacherPhoto = $("#inp-teacherPhoto").val().trim();
    }
    //hàm kiểm tra thông tin dữ liệu danh sách các khoá học :
    function validateDataCourse(paramDataCourseObject){
        "use strict"
        if(paramDataCourseObject.courseCode === ""){
            alert("Hãy nhập Course Code ");
            return false;
        }
        if(paramDataCourseObject.courseName === ""){
            alert("Hãy nhập Course Name");
            return false;
        }
        if(paramDataCourseObject.coverImage === ""){
            alert("Hãy nhập Cover Image");
            return false;
        }
        if (isNaN(paramDataCourseObject.discountPrice) == false){
            alert("Discount Price phải là 1 số");
            return false;
        }
        if (isNaN(paramDataCourseObject.price) == false){
            alert("Discount Price phải là 1 số");
            return false;
        }
        if(paramDataCourseObject.discountPrice === ""){
            alert("Hãy nhập Discount Price");
            return false;
        }
        if(paramDataCourseObject.duration === ""){
            alert("Hãy nhập Duration");
            return false;
        }
        if(paramDataCourseObject.isPopular === ""){
            alert("Hãy nhập isPopular");
            return false;
        }
        if(paramDataCourseObject.isTrending === ""){
            alert("Hãy nhập isTrending");
            return false;
        }
        if(paramDataCourseObject.level === ""){
            alert("Hãy nhập level");
            return false;
        }
        if(paramDataCourseObject.teacherName === ""){
            alert("Hãy nhập Teacher Name");
            return false;
        }
        if(paramDataCourseObject.price === ""){
            alert("Hãy nhập price");
            return false;
        }
        if(paramDataCourseObject.teacherPhoto === ""){
            alert("Hãy nhập Teacher Photo");
            return false;
        }
        return true;
    }
    function getDataEditToCourseObject(paramDataCourseObject){
        "use strict"
        var gStt = "";
        for (var vIndex = 0; vIndex < gCoursesDB.courses.length ; vIndex ++){
            if (gCoursesDB.courses[vIndex].id === paramDataCourseObject.id){
                gStt = vIndex ;
                console.log(vIndex);
                gCoursesDB.courses[gStt].courseCode = paramDataCourseObject.courseCode;
                gCoursesDB.courses[gStt].courseName = paramDataCourseObject.courseName;
                gCoursesDB.courses[gStt].coverImage = paramDataCourseObject.coverImage;
                gCoursesDB.courses[gStt].discountPrice = paramDataCourseObject.discountPrice;
                gCoursesDB.courses[gStt].duration = paramDataCourseObject.duration;
                gCoursesDB.courses[gStt].isPopular = paramDataCourseObject.isPopular;
                gCoursesDB.courses[gStt].isTrending = paramDataCourseObject.isTrending;
                gCoursesDB.courses[gStt].level = paramDataCourseObject.level;
                gCoursesDB.courses[gStt].price = paramDataCourseObject.price;
                gCoursesDB.courses[gStt].teacherName = paramDataCourseObject.teacherName;
                gCoursesDB.courses[gStt].teacherPhoto = paramDataCourseObject.teacherPhoto;
            }
        }
        //Load lại bảng :
        loadDataCourseToTable();
        alert("Cập nhật thông tin thành công")
        //Tắt modal :
        $("#edit-modal").modal("hide");

    }
    //Hàm xử lý sự kiện khi bấm nút delete:
    function onBtnDeleteClick(paramButton){
        "use strict"
        $("#delete-modal").modal("show");
        var vRowClick = $(paramButton).closest("tr");
        var vTable = $("#table").DataTable();
        var vDataRow = vTable.row(vRowClick).data();
        console.log(vDataRow);
        //Load thông tin khoá học lên form delete:
        $("#span").html(vDataRow.id);
    }
    //Hàm xử lý sự kiện khi bấm nút đồng ý trong modal delete :
    function onBtnAccecptDeleteClick(){
        "use strict"
        //Bước 1: Tạo object :
        var vCourseObject = {
            id:""
        }
        //Bước 2 : Thu thập dữ liệu
        getCourseObject(vCourseObject);
        console.log(vCourseObject);
        //Bước 3 : Kiểm tra dữ liệu (không cần);
        //Bước 4 : Xử lý dữ liệu :
        deleteCourse(vCourseObject);
    }
    function getCourseObject(paramCourseObject){
        "use strict"
        paramCourseObject.id = parseInt($("#span").html());
        console.log(paramCourseObject.id);
    }
    function deleteCourse(paramCourseObject){
        "use strict"
        var vCourse = gCoursesDB.courses;
        console.log(vCourse);
        var vFiltered = [];
        vFiltered = gCoursesDB.courses.filter(function(paramCourse){
            return (paramCourse.id !== paramCourseObject.id);
        });
        console.log(vFiltered);
        //Load lại bảng :
        alert("Xoá khoá học thành công");
        vDataTable.clear();
        vDataTable.rows.add(vFiltered);
        vDataTable.draw();
        $("#delete-modal").modal("hide");
    }
});